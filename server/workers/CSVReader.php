<?php

namespace app\workers;

use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\base\Exception;

class CSVReader extends BaseObject {
  private $_file;
  private $_list;
  
  function load($path): CSVReader {
    if (!file_exists($path)) {
      throw new InvalidArgumentException("File for path {$path} does not exist.");
    }
    
    $this->_file = $path;
    
    return $this;
  }
  
  function parse(): CSVReader {
    $file = fopen($this->_file, 'r');
    if ($file === false) {
      throw new Exception("Open data file failed.");
    }
    
    $list = [];
    while(($data = fgetcsv($file, 0, ';')) !== false) {
//      print_r($data);
      $list[] = $data;
    }
    
    $this->_list = $list;
    
    return $this;
  }
  
  function getRows(): array {
    return $this->_list;
  }
}