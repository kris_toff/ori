<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductCatalog extends ActiveRecord {
  public $name;
  public $code;
  
  public function getProduct() {
    return $this->hasMany(Product::class, ['code' => 'product_code']);
  }
  
  public function fields() {
    $fields = parent::fields();
    
    return array_merge([
      'name',
      'code'
    ], $fields);
  }
}