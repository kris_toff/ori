<?php

namespace app\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord {
  public $discount;
  public $page;
  public $is_sale;
  public $points;
  public $price_wholesale;
  public $price_distribution;
  public $price;
  public $type;
  
  public function getProducts() {
    return $this->hasMany(ProductCatalog::class, ['product_code' => 'code']);
  }
  
  public function fields(): array {
    $fields = parent::fields();
    
    return array_merge([
      'discount', 'page', 'is_sale', 'points', 'price_wholesale', 'price_distribution', 'price', 'type'
    ], $fields);
  }
}