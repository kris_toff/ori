<?php

namespace app\models;

use yii\web\UploadedFile;
use yii\base\Model;
use app\workers\CSVReader;
use app\models\{Product, ProductCatalog};

class ProductLoadForm extends Model {
  public $file;
  public $catalog;
  public $year;
  public $type;
  
  public function rules(): array {
    return [
      [['catalog', 'year', 'type', 'file'], 'required'],
      ['catalog', 'integer', 'min' => 1, 'max' => 17],
      ['type', 'boolean'],
      ['year', 'integer'],
      ['file', 'file', 'extensions' => 'csv', 'mimeTypes' => ['text/csv', 'application/vnd.ms-excel', 'text/plain'], 'checkExtensionByMimeType' => false],
    ];
  }
  
  public function save(CSVReader $reader): bool {
    $reader
      ->load($this->file->tempName)
      ->parse();
    
    $db = \Yii::$app->db;
    $catalog = $this->catalog;
    $year = $this->year;
    $type = $this->type;
    
    $list = $reader->rows;
    $sourceProductCodes = Product::find()->select('code')->column();
    $acceptedProductCodes = array_column($list, 5, 0);
    
    // check basic product table for existing product name
    $diffCodes = array_diff_key($acceptedProductCodes, array_flip($sourceProductCodes));
    if (count($diffCodes)) {
      $queryData = array_map(function($k, $v){ return [(int)$k, trim($v)]; }, array_keys($diffCodes), $diffCodes);

      $query = $db->createCommand();
      $query->batchInsert(Product::tableName(), ['code', 'name'], $queryData);
      $query->execute();
    }
    
    // add/update catalog price 
    $query = $db->createCommand();
    $query->delete(ProductCatalog::tableName(), ['catalog' => $this->catalog, 'year' => $this->year, 'product_code' => array_keys($acceptedProductCodes)]);
    $query->execute();
//    print_r($list);
    $queryData = array_map(function($a) use($catalog, $year, $type) {
      return [$a[0], $this->catalog, $this->year, $a[1], $a[2], strlen($a[4]) ? 1 : 0, $a[6], $a[7], $a[8], $a[9], $type]; 
    }, $list );
    $query = $db->createCommand();
    $query->batchInsert(ProductCatalog::tableName(), ['product_code', 'catalog', 'year', 'discount', 'page', 'is_sale', 'points', 'price_wholesale', 'price_distribution', 'price', 'type'], $queryData);
    $query->execute();
    
//    return $this->file->saveAs('test.file');
    return true;
  }
  
}