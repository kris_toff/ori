<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

class Procedure extends ActiveRecord{
  public function rules(){
    return [
      [['name', 'content'], 'required'],
      [['name', 'content'], 'trim'],
    ];
  }
  
  public function behaviors(){
    return [
      [
        'class' => TimestampBehavior::class,
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ]
      ],
      [
        'class' => SluggableBehavior::class,
        'attribute' => 'name',
        'ensureUnique' => true,
        'slugAttribute' => 'link',
        'immutable' => false
      ]
    ];
  }
}