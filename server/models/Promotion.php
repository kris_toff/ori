<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

class Promotion extends ActiveRecord {
  function rules() {
    return [
      [['name', 'content', 'validity_date_from', 'validity_date_to'], 'required'],
      ['name', 'string', 'min' => 2, 'max' => 60],
      ['content', 'string', 'min' => 25, 'max' => 25999],
      [['validity_date_from', 'validity_date_to'], 'integer'],
      ['validity_date_to', 'validateDate'],
    ];
  }
  
  function validateDate($attribute, $params) {
    $relativeDate = new \DateTime("@{$this->validity_date_from}");
    $greaterDate = new \DateTime("@{$this->$attribute}");
    
    if ($relativeDate > $greaterDate) {
      $this->addError($attribute, 'Дата завершения должна быть больше даты начала.');
      return false;
    }
    
    return true;
  }
  
  function behaviors() {
    return [
      [
        'class' => TimestampBehavior::class,
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ]
      ],
      [
        'class' => SluggableBehavior::class,
        'attribute' => 'name',
        'ensureUnique' => true,
        'slugAttribute' => 'link',
        'immutable' => false,
      ]
    ];
  }
}
