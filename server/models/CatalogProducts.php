<?php

namespace app\models;

use yii\base\Model;
use yii\db\ActiveQuery;
use yii\web\BadRequestHttpException;

use app\models\Product;
use app\models\ProductCatalog;

class CatalogProducts extends Model {
  public $catalog;
  public $year;
  public $is_all;
  
  public function rules(): array {
    return [
      [['catalog', 'year'], 'required'],
      ['catalog', 'integer', 'min' => 1, 'max' => 17],
      ['year', 'integer', 'min' => 2016, 'max' => 2019],
      ['is_all', 'boolean'],
      ['is_all', 'default', 'value' => 0],
    ];
  }
  
  public function load($data, $formName = null): CatalogProducts {
    if (!parent::load($data, $formName)) {
      throw new BadRequestHttpException('Data format does not match needed.');
    }
    
    return $this;
  }
  
  public function products($validation = true): ActiveQuery {
    if ($validation && !$this->validate()) {
      throw new BadRequestHttpException('Accepted data parameters is wrong.');
    }
    
    $tpc = ProductCatalog::tableName();
    $tp = Product::tableName();
    
    $models = Product::find()
      ->select("{$tp}.*");
    
    if ( (int)$this->is_all ) {
      $models
        ->addSelect('u.*')
        ->leftJoin([
          'u' => ProductCatalog::find()->where([
            'catalog' => (int)$this->catalog,
            'year' => (int)$this->year
          ])
        ], 
                   "{$tp}.[[code]] = {{u}}.[[product_code]]");
    } else {
      $models
        ->addSelect("{$tpc}.*")
        ->joinWith('products', false, 'RIGHT JOIN')
        ->where([
          "{$tpc}.catalog" => (int)$this->catalog,
          "{$tpc}.year" => (int)$this->year
        ]);
    }
    
    return $models;
  }
  
  public function formName(): string {
    return '';
  }
}