<?php

use yii\db\Migration;

/**
 * Handles the creation of table `procedure`.
 */
class m181216_183443_create_procedure_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('procedure', [
          'id' => $this->primaryKey(),
          'name' => $this->string(60),
          'content' => $this->text(),
          'link' => $this->string(125),
          'created_at' => $this->integer(11),
          'updated_at' => $this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('procedure');
    }
}
