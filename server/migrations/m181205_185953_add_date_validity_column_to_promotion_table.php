<?php

use yii\db\Migration;

/**
 * Handles adding date_validity to table `promotion`.
 */
class m181205_185953_add_date_validity_column_to_promotion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('promotion', 'validity_date_from', $this->integer(11));
      $this->addColumn('promotion', 'validity_date_to', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('promotion', 'validity_date_from');
      $this->dropColumn('promotion', 'validity_date_to');
    }
}
