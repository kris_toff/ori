<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m181220_113426_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
          'code' => $this->integer()->notNull(),
            'name' => $this->string(),
          'in-date' => $this->integer(),
          'out-date' => $this->integer(),
        ]);
      
      $this->addPrimaryKey('pk_product_code', 'product', 'code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropPrimaryKey('pk_product_code', 'product');
      
        $this->dropTable('product');
    }
}
