<?php

use yii\db\Migration;

/**
 * Handles adding link to table `promotion`.
 */
class m181202_223531_add_link_column_to_promotion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('promotion', 'link', $this->string(60));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('promotion', 'link');
    }
}
