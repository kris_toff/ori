<?php

use yii\db\Migration;

/**
 * Handles adding type to table `product_catalog`.
 */
class m181224_105042_add_type_column_to_product_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_catalog', 'type', $this->boolean()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_catalog', 'type');
    }
}
