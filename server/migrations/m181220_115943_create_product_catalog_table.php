<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_catalog`.
 */
class m181220_115943_create_product_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_catalog', [
            'product_code' => $this->integer(),
            'catalog' => $this->integer(),
            'year' => $this->integer(),
          'discount' => $this->float(),
          'page' => $this->integer(),
          'is_sale' => $this->boolean()->defaultValue(false),
          'points' => $this->integer(),
          'price_wholesale' => $this->float(),
          'price_distribution' => $this->float(),
          'price' => $this->float(),
          
        ]);
      
      $this->addPrimaryKey('pk_product_catalog_product_code_catalog_year', 'product_catalog', ['product_code', 'catalog', 'year']);
      
      $this->addForeignKey('fk_product_catalog_product_code_catalog_year', 'product_catalog', 'product_code', 'product', 'code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropForeignKey('fk_product_catalog_product_code_catalog_year', 'product_catalog');
      $this->dropPrimaryKey('pk_product_catalog_product_code_catalog_year', 'product_catalog');
      
        $this->dropTable('product_catalog');
    }
}
