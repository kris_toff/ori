<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promotion`.
 */
class m181202_212911_create_promotion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('promotion', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'content' => $this->text(),
          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('promotion');
    }
}
