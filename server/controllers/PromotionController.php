<?php

namespace app\controllers;

use yii\rest\Controller;

use app\models\Promotion;

class PromotionController extends Controller{
  public function actionIndex(){
    return Promotion::find()
      ->select(['id', 'name', 'link', 'validity_date_from', 'validity_date_to'])
      ->all();
  }
  
  public function actionView($id){
    return Promotion::find()
      ->where(['id' => $id])
      ->one();
  }
  
  public function actionCreate() {
    $request = \Yii::$app->request;
    
    $model = new Promotion();
    $model->load($request->post(), '');
    if (!$model->save()){
      return $model->errors;
    }
    
    return $model;
  }
  
  public function actionUpdate($id){
    $request = \Yii::$app->request;
    
    $model = Promotion::findOne(['id' => $id]);
    if ($model) {
      $model->load($request->post(), '');
      $model->save();
    }
    
    return $model;
  }
  
}