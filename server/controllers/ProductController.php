<?php

namespace app\controllers;

use yii\rest\Controller;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\models\CatalogProducts;
use app\models\ProductLoadForm;
use yii\web\ServerErrorHttpException;

class ProductController extends Controller {
  public function actionIndex() {
    return (new CatalogProducts())
      ->load(\Yii::$app->request->get())
      ->products()
      ->all();
  }
  
  public function actionLoad() {
    $request = \Yii::$app->request;
    
    $model = new ProductLoadForm();
    $model->load($request->post(), '');
    $model->file = UploadedFile::getInstanceByName('file');
    
    if (!$model->validate()) {
      return $model->errors;
    } elseif (!\Yii::$container->invoke([$model, 'save'])) {
      throw new ServerErrorHttpException('Save data failed.');
    }
    
    return [];
  }
}