<?php

namespace app\controllers;

use yii\rest\Controller;
use app\models\Procedure;

class ProcedureController extends Controller {
  public function actionIndex(){
    return Procedure::find()
      ->select(['id', 'name', 'link', 'created_at', 'updated_at'])
      ->all();
  }
  
  public function actionCreate(){
    $request = \Yii::$app->request;
    
    $model = new Procedure();
    $model->load($request->post(), '');
    if (!$model->save()) {
      return [];
    }
    
    return $model;
  }
  
  public function actionUpdate($id) {
    $request = \Yii::$app->request;
    
    $model = $this->findModel($id);
    $model->load($request->post(), '');
    $model->save();
    
    return $model;
  }
  
  public function actionView($id){
    $model = $this->findModel($id);
    
    return $model;
  }
  
  private function findModel($id) {
    return Procedure::findOne(['id' => $id]);
  }
}