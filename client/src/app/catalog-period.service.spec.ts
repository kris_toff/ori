import { TestBed } from '@angular/core/testing';

import { CatalogPeriodService } from './catalog-period.service';

describe('CatalogPeriodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CatalogPeriodService = TestBed.get(CatalogPeriodService);
    expect(service).toBeTruthy();
  });
});
