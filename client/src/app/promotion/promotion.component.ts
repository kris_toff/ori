import { Component, OnInit } from '@angular/core';
import { CreatePromotionDialogComponent } from "../create-promotion-dialog/create-promotion-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";

import { PromotionService } from "../promotion.service";
import { Promotion } from "../promotion";

import _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.sass'],
})
export class PromotionComponent implements OnInit {
  public promotions$: Observable<Promotion[]>;

  constructor(private dialog: MatDialog, private promotion: PromotionService) { }

  ngOnInit() {
    this.promotions$ = this.promotion
      .getAll<Promotion>()
      .pipe(
//        tap(x => console.log(x)),
        map(x => _.orderBy(x, ['validity_date_to', 'validity_date_from'], ['desc', 'desc']))
      );
  }

  addPromotionBox() {
    const dialogRef = this.dialog.open(CreatePromotionDialogComponent, {
      minWidth: '70%',
      data: {
        action: 'create'
      }
    });
    
    dialogRef.afterClosed()
      .subscribe(result => {
        if (!(_.isNil(result) || _.isString(result) && !result.length)){
          this.promotion.save(result);
        }
      });
  }
  
  expirationDate(date): boolean {
    return moment().isAfter(date * 1000);
  }
}