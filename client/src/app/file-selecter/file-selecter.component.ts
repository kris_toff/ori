import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

import _ from "lodash";

@Component({
  selector: 'app-file-selecter',
  templateUrl: './file-selecter.component.html',
  styleUrls: ['./file-selecter.component.sass'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, multi: true, useExisting: forwardRef(() => FileSelecterComponent)}
  ]
})
export class FileSelecterComponent implements OnInit, ControlValueAccessor {
  private file: File;
  
  private propagateChange: any = () => {};
  private propagateTouched: any = () => {};

  constructor() { }

  ngOnInit() {
  }

  selectNewFile($event, target) {
    let files: FileList = _.get(target, 'files');
    let file: File = _.get(files, 0);
    console.log('change', $event, target, files);
    
    if (file !== undefined) {
      let reader = new FileReader();

      reader.onload = this.onloadFile.bind(this, reader, file);
      reader.readAsArrayBuffer(target.files[0]);
    }
    
    this.file = file;
    this.propagateChange(file);
  }
  
  private onloadFile(reader: FileReader, file: File): void {
    let f: ArrayBuffer | string = reader.result;
  }
  
  get isLoaded(): boolean {
    return !_.isNil(this.file);
  }
  
  get fileName(): string {
    return _.get(this.file, 'name');
  }
  
  get fileSize(): string {
    return _.get(this.file, 'size');
  }
  
  writeValue(obj: any): void {
    this.file = obj;
  }
  
  registerOnChange(fn: any): void {
//    console.log('reg change');
    this.propagateChange = fn;
  }
  
  registerOnTouched(fn: any): void {
//    console.log('reg touch');
    this.propagateTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    console.log('disable');
  }
}
