export interface Product {
  code: number;
  name: string;
  discount: number;
  page: number;
  price: number;
  price_wholesale: number;
  price_distribution: number;
}