import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Observable, BehaviorSubject, timer, EMPTY } from "rxjs";
import { filter, map, mergeMap, switchMap, share, tap } from "rxjs/operators";

import { RemoteDataSettings } from "./remote-data-settings";
import { RemoteData } from "./remote-data";

import _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class RemoteDataService {
  protected data$: BehaviorSubject<RemoteData[]>;
  protected url!: string;

  constructor(public http: HttpClient) {
    this.data$ = new BehaviorSubject<RemoteData[]>([]);
    
     timer(0, RemoteDataSettings.dataUpdateTime)
      .subscribe(this.updatePromotionList.bind(this));
  }
  
  getAll<T extends RemoteData>(): Observable<Array<T>> {
    return <Observable<T[]>>(this.data$)
      .asObservable();
  }
  
  getItem<T extends RemoteData>(code: string): Observable<T> {
    return this.data$
      .asObservable()
      .pipe(
        map( (v: T[]): T => _.find(v, {link: code}) ),
        filter(x => ! (_.isNil(x) || _.isEmpty(x)) ),
//        tap(x => console.log(x)),
        map( (v: T) => v.id ),
        switchMap(id => this.http.get<T>(this.url + '/' + id))
      );
  }
  
  private updatePromotionList(): void {
    this.http.get<RemoteData[]>(this.url)
      .subscribe(result => {
        this.data$.next(result);
      });
  }
  
  public save(data: RemoteData): Observable<RemoteData> {
    if (_.isNil(data)) {
      return EMPTY;
    }
    
    let request;
    if (_.has(data, 'id')) {
      // exists id parameter that means entry is not new
      request = this.http.put<RemoteData>(this.url + '/' + data.id, data);
    } else {
      // entry is new, create it
      request = this.http.post<RemoteData>(this.url, data);
    }
    
    request = request.pipe(share());
    request.subscribe();
    
    return request;
  }
}
