import { Promotion } from "./promotion";

export interface RemoteDialogData {
  action: string;
  content?: Promotion;
}