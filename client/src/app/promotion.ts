import { RemoteData } from "./remote-data";

export interface Promotion extends RemoteData {
  id?: number;
  
  validity_date_from: number;
  validity_date_to: number;
}