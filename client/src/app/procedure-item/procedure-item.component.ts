import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";

import { map, tap, switchMap } from "rxjs/operators";

import { CreateProcedureDialogComponent } from "../create-procedure-dialog/create-procedure-dialog.component";
import { ProcedureService } from "../procedure.service";
import { Procedure } from "../procedure";

import _ from "lodash";

@Component({
  selector: 'app-procedure-item',
  templateUrl: './procedure-item.component.html',
  styleUrls: ['./procedure-item.component.sass']
})
export class ProcedureItemComponent implements OnInit, OnDestroy {
  public data: Procedure;

  constructor(private route: ActivatedRoute, 
  private router: Router,
  private procedure: ProcedureService,
  private dialog: MatDialog) { }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        map(o => o.get('name')),
        switchMap(o => this.procedure.getItem<Procedure>(o))
      )
      .subscribe(this.changeContent.bind(this));
  }
  
  ngOnDestroy() {
    console.log('destroy');
  }

  private changeContent(c: Procedure): void {
//  console.log(c);
    this.data = c;
  }
  
  public openEditBox(){
    const dialogRef = this.dialog.open(CreateProcedureDialogComponent, {
      minWidth: '70%',
      data: {
        action: 'update',
        data: this.data
      }
    });
    
    dialogRef.afterClosed()
      .subscribe(data => {
        let ex_data: Procedure = _.assign(this.data, data);
        
        this.procedure.save(ex_data)
          .subscribe(result => {
            this.router.navigate(['/procedure', result.link]);
          });
      });
  }
}
