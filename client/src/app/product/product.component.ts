import { Component, Directive, OnInit, AfterViewInit, OnDestroy, ViewChildren, ViewChild, QueryList, EventEmitter, ElementRef, ViewContainerRef } from '@angular/core';

import { ProductService } from "../product.service";
import { CatalogPeriodService } from "../catalog-period.service";
import { LoadProductsDialogComponent } from "../load-products-dialog/load-products-dialog.component";
import { FilterDataSourceFieldDirective } from "../filter-data-source-field.directive";
import { UpdateDataSourceFieldDirective } from "../update-data-source-field.directive";

import { of, from, merge, fromEvent, combineLatest, BehaviorSubject, Observable } from "rxjs";
import { switchMap, mergeAll, mergeMap, tap, pluck, map, distinctUntilChanged, startWith, throttleTime, sampleTime, debounceTime, auditTime, shareReplay, finalize } from "rxjs/operators";
import { MatSort } from "@angular/material";
import { MatCheckbox } from "@angular/material/checkbox";
import { MatInput } from "@angular/material/input";
import { MatTableDataSource } from "@angular/material/table";
import { MatSelect, MatSelectChange } from "@angular/material/select";
import { MatDialog } from "@angular/material/dialog";

import { Product } from "../product";

import _ from "lodash";
import * as moment from "moment";

const DISPLAY_TABLE_COLUMNS = ['code', 'name', 'page', 'discount', 'points', 'price_wholesale', 'price_distribution', 'price'];


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass'],
})
export class ProductComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(UpdateDataSourceFieldDirective, {read: MatSelect})
  public updaterSelect: QueryList<MatSelect>;
  @ViewChildren(UpdateDataSourceFieldDirective, {read: MatCheckbox})
  public updaterCheckbox: QueryList<MatCheckbox>;
  
  @ViewChildren(FilterDataSourceFieldDirective, {read: MatInput})
  public filters: QueryList<MatInput>;
  
  public isOut: boolean = false;
  public catalogRange: number[];
  public yearRange: number[];
  
  public catalogSelect: number;
  public yearSelect: number;
  public filterWord: string;
  
  public source$: Observable<Product[]>;
  public sortingData$: BehaviorSubject<SortTableInterface>;
  public filteringData$: Observable<string>;
  public data$: Observable<Product[]>;
  
  public displayedColumns: string[] = DISPLAY_TABLE_COLUMNS;
  public isDataSourceUpdating: boolean = false;


  constructor(private product: ProductService,
  private period: CatalogPeriodService,
  private dialog: MatDialog) { }

  ngOnInit() {
    let year: number = moment().year();
    
    // generate year and catalog ranges
    this.catalogRange = _.range(1, 18);
    this.yearRange = _.range(2016, year + 2);
    
    // set current value
    this.catalogSelect = this.period.current();
    this.yearSelect = year;
    
    // observables
    this.sortingData$ = new BehaviorSubject<SortTableInterface>({} as SortTableInterface);
    this.filteringData$ = new BehaviorSubject<string>('');
    this.source$ = this.product.getAll();
    
    this.updateProducts();
    
    this.data$ = this.source$;
  }
  
  ngAfterViewInit(): void {
//    this.sortingData$.subscribe(x => console.log(x));
//    console.log(this.updaterSelect, this.updaterCheckbox, this.filters);
    let updators: any[] = [],
      fls: any[] = [];
    this.updaterSelect.map(o => updators.push(o.selectionChange));
    this.updaterCheckbox.map(o => updators.push(o.change));
    
    this.filters.map( o => fls.push( _.get(o, ['ngControl', 'update']) ));
    this.filteringData$ = from( fls )
      .pipe(
        mergeAll(),
        startWith(''),
        map( (str: string) => str.trim() ),
        shareReplay()
      );
//    console.log(updators, fls);

    let sorting$ = this.sortingData$.pipe( shareReplay() );
    
    from( updators )
      .pipe( mergeAll() )
      .subscribe( this.updateProducts.bind(this) );

//    this.filteringData$.subscribe(x => console.log(x));

    this.data$ = this.source$
      .pipe(
        map(x => _.map(x, v => {
            ['page', 'code'].forEach(o => _.get(v, o) ? _.update(v, o, _.toInteger) : _.noop());
            return v;
          })),
        switchMap(data => this.filteringData$.pipe(
          distinctUntilChanged(),
          auditTime(350),
          map(str => str.length ? _.filter(data, v => _.some(v, u => new RegExp(str, 'i').test(u))) : data)
        )),
        switchMap(data => sorting$.pipe(
          map(srt => _.isEmpty(srt) || _.some(srt, param => !param) ? data : _.orderBy(data, [srt.active], [srt.direction]))
        )),
        tap(_ => this.isDataSourceUpdating = false)
      )
//      .subscribe(x => console.log(x))
      ;
  }
  
  ngOnDestroy(): void {
//    console.log('destroy');
//    this.selectors.map();
  }
  
  openNewProductsBox(): void {
    const dialog = this.dialog.open(LoadProductsDialogComponent, {
      disableClose: true,
      position: {top: '25px'}
    });
  }
  
  private updateProducts(): void {
    this.isDataSourceUpdating = true;
    this.product.update(this.catalogSelect, this.yearSelect, this.isOut);
  }
  
  public clearFilter(): void {
    this.filterWord = '';
    _.invoke(this.filters, 'first.ngControl.update.emit', this.filterWord);
  }
  
  getPercent(v: number): string {
    return _.isNil(v) ? '' : _.floor(v * 100, 1) + '%';
  }
  
  sortData(params: SortTableInterface): void {
    this.sortingData$.next(params);
  }
}

export interface SortTableInterface {
  active: string;
  direction: 'asc' | 'desc';
}