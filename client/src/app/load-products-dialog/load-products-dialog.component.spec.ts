import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadProductsDialogComponent } from './load-products-dialog.component';

describe('LoadProductsDialogComponent', () => {
  let component: LoadProductsDialogComponent;
  let fixture: ComponentFixture<LoadProductsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadProductsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadProductsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
