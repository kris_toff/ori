import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

import { CatalogPeriodService } from "../catalog-period.service";
import { ProductService } from "../product.service";

import _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'app-load-products-dialog',
  templateUrl: './load-products-dialog.component.html',
  styleUrls: ['./load-products-dialog.component.sass']
})
export class LoadProductsDialogComponent implements OnInit {
  public catalogRange: number[];
  public yearRange: number[];
  
  public loadForm: FormGroup;
  public catalog: number;
  
  public ptypes = {1: 'Из основного каталога', 0: 'Из дополнительного каталога'};
  
  constructor(private period: CatalogPeriodService,
  private fb: FormBuilder,
  private product: ProductService) { }

  ngOnInit() {
    let cy: number = moment().year();
    
    this.catalogRange = _.range(1, 18);
    this.yearRange = _.range(2016, cy + 2, 1);
    
    this.loadForm = this.fb.group({
      catalog: [''],
      year: [''],
      type: [''],
      file: [null]
    });
    this.loadForm.patchValue({catalog: this.period.current(), year: cy});
  }

  loadFile() {
    this.product.load(this.loadForm.value);
    console.log(this.loadForm.value);
  }
}
