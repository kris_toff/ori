import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wrap',
  templateUrl: './wrap.component.html',
  styleUrls: ['./wrap.component.sass']
})
export class WrapComponent implements OnInit {
  public links = [
    {link: '/', label: 'Домой', options: {exact: true}},
    {link: '/promotion', label: 'Акции', options: {}},
    {link: '/procedure', label: 'Процедуры', options: {}},
    {link: '/product', label: 'Товары', options: {}}
  ];

  constructor() { }

  ngOnInit() {
  }

}
