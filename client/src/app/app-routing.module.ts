import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from "./welcome/welcome.component";
import { PromotionComponent } from "./promotion/promotion.component";
import { PromotionItemComponent } from "./promotion-item/promotion-item.component";
import { ProcedureItemComponent } from "./procedure-item/procedure-item.component";
import { ProcedureComponent } from "./procedure/procedure.component";
import { WrapComponent } from "./wrap/wrap.component";
import { ProductComponent } from "./product/product.component";

const routes: Routes = [
  {path: '', component: WelcomeComponent, pathMatch: 'full'},
  {path: '', component: WrapComponent, children: [
    {path: 'promotion', component: PromotionComponent, children: [
      {path: ':name', component: PromotionItemComponent},
    ]},
    {path: 'procedure', component: ProcedureComponent, children: [
      {path: ':name', component: ProcedureItemComponent},
    ]},
    {path: 'product', component: ProductComponent},
  ]},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
