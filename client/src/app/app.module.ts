import { BrowserModule } from '@angular/platform-browser';
import { NgModule, forwardRef, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule, NG_VALUE_ACCESSOR } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatMomentDateModule } from "@angular/material-moment-adapter";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MatSidenavModule } from "@angular/material/sidenav";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatListModule } from "@angular/material/list";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ProcedureComponent } from './procedure/procedure.component';
import { PromotionComponent } from './promotion/promotion.component';
import { WelcomeComponent } from './welcome/welcome.component';

import { FlexLayoutModule } from "@angular/flex-layout";
import { WrapComponent } from './wrap/wrap.component';
import { PromotionItemComponent } from './promotion-item/promotion-item.component';
import { CreatePromotionDialogComponent } from './create-promotion-dialog/create-promotion-dialog.component';

import { EditorModule } from "@tinymce/tinymce-angular";
import { ProcedureItemComponent } from './procedure-item/procedure-item.component';
import { CreateProcedureDialogComponent } from './create-procedure-dialog/create-procedure-dialog.component';
import { ProductComponent } from './product/product.component';
import { LoadProductsDialogComponent } from './load-products-dialog/load-products-dialog.component';
import { FileSelecterComponent } from './file-selecter/file-selecter.component';
import { FilterDataSourceFieldDirective } from './filter-data-source-field.directive';
import { UpdateDataSourceFieldDirective } from './update-data-source-field.directive';

import { registerLocaleData } from "@angular/common";
import localeRu from "@angular/common/locales/ru";

registerLocaleData( localeRu, 'ru' );

@NgModule({
  declarations: [
    AppComponent,
    ProcedureComponent,
    PromotionComponent,
    WelcomeComponent,
    WrapComponent,
    PromotionItemComponent,
    CreatePromotionDialogComponent,
    ProcedureItemComponent,
    CreateProcedureDialogComponent,
    ProductComponent,
    LoadProductsDialogComponent,
    FileSelecterComponent,
    FilterDataSourceFieldDirective,
    UpdateDataSourceFieldDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    
    FlexLayoutModule,
    
    EditorModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'ru'}
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    CreatePromotionDialogComponent,
    CreateProcedureDialogComponent,
    LoadProductsDialogComponent,
  ]
})
export class AppModule { }

