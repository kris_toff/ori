import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";

import { ProcedureService } from "../procedure.service";
import { CreateProcedureDialogComponent } from "../create-procedure-dialog/create-procedure-dialog.component";

import { Observable } from "rxjs";
import { Procedure } from "../procedure";

@Component({
  selector: 'app-procedure',
  templateUrl: './procedure.component.html',
  styleUrls: ['./procedure.component.sass']
})
export class ProcedureComponent implements OnInit {
  public procedure$: Observable<Procedure[]>;

  constructor(private procedure: ProcedureService, 
  private dialog: MatDialog) { }

  ngOnInit() {
    this.procedure$ = this.procedure.getAll<Procedure>();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateProcedureDialogComponent, {
      minWidth: '70%'
    });
    
    dialogRef.afterClosed()
      .subscribe(result => {
        this.procedure.save(result);
      });
  }
}
