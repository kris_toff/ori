import { Directive } from '@angular/core';

@Directive({
  selector: '[appFilterDataSourceField], [app-filter-data-source-field]'
})
export class FilterDataSourceFieldDirective {

  constructor() { }

}
