import { Directive } from '@angular/core';

@Directive({
  selector: '[appUpdateDataSourceField], [app-update-data-source-field]'
})
export class UpdateDataSourceFieldDirective {

  constructor() { }

}
