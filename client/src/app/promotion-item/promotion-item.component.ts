import { Component, OnInit } from '@angular/core';
import { CreatePromotionDialogComponent } from "../create-promotion-dialog/create-promotion-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";

import { map, tap, filter, defaultIfEmpty, switchMap } from "rxjs/operators";
import { PromotionService } from "../promotion.service";
import { Promotion } from "../promotion";

import _ from "lodash";

@Component({
  selector: 'app-promotion-item',
  templateUrl: './promotion-item.component.html',
  styleUrls: ['./promotion-item.component.sass']
})
export class PromotionItemComponent implements OnInit {
  public data: Promotion;

  constructor(private dialog: MatDialog, 
  private route: ActivatedRoute, 
  private promotion: PromotionService, 
  private router: Router) { }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        map(o => o.get('name')),
        switchMap(o => this.promotion.getItem<Promotion>(o))
      )
//      .pipe( tap(x => console.log(x)) )
      .subscribe(this.changeContent.bind(this));
  }
  
  private changeContent(c: Promotion) {
    this.data = c;
  }

  editPromotionBox() {
    const dialogRef = this.dialog.open(CreatePromotionDialogComponent, {
      minWidth: '70%',
      data: {
        action: 'update',
        data: this.data
      }
    });
    
    dialogRef.afterClosed()
      .subscribe(result => {
        this.promotion.save(result)
//          .pipe( tap(x => console.log(x, typeof x)) )
          .subscribe(x => {
//            console.log('result', x);
            this.router.navigate(['/promotion', x.link]);
          });
//        console.log(result);
      });
  }
}