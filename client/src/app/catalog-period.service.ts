import { Injectable } from '@angular/core';

import * as moment from "moment";
import _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class CatalogPeriodService {
  private currentCatalog;

  constructor() {
    
  }
  
  current(): number {
    if (_.isNil(this.currentCatalog)) {
      this.currentCatalog = _.clamp(Math.floor(moment().week() / 3), 1, 17);
    }
    
    return this.currentCatalog;
  }
}