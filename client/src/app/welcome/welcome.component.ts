import { Component, OnInit } from '@angular/core';

import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.sass']
})
export class WelcomeComponent implements OnInit {
  sites = {ua: 'Украина', ru: 'Россия', md: 'Молдова', kz: 'Казахстан', by: 'Беларусь'};

  constructor(private iconRegister: MatIconRegistry,
  private sanitizer: DomSanitizer) { }

  ngOnInit() {
  	this.iconRegister.addSvgIcon('logo', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/oriflame_logo.svg'));
  }
}
