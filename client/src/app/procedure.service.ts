import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { RemoteDataService } from "./remote-data.service";

@Injectable({
  providedIn: 'root'
})
export class ProcedureService extends RemoteDataService {
  protected url: string = '/api/procedures';

  constructor(http: HttpClient) {
      super(http);
  }
}