export interface RemoteData {
  id?: number;
  name: string;
  content?: string;
  link: string;
  created_at: number;
  updated_at: number;
}
