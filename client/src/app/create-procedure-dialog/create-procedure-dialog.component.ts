import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { MAT_DIALOG_DATA } from "@angular/material";

import { Procedure } from "../procedure";
import { RemoteDialogData } from "../remote-dialog-data";

import _ from "lodash";

@Component({
  selector: 'app-create-procedure-dialog',
  templateUrl: './create-procedure-dialog.component.html',
  styleUrls: ['./create-procedure-dialog.component.sass']
})
export class CreateProcedureDialogComponent implements OnInit {
  public procedureForm: FormGroup;

  constructor(private fb: FormBuilder,
  @Inject(MAT_DIALOG_DATA)public data: {data: RemoteDialogData, action: 'update' | 'create'}) { }

  ngOnInit() {
    this.procedureForm = this.fb.group({
      name: [''],
      content: ['']
    });
    
    if (_.has(this.data, 'data')){
      let data: Procedure = _.clone(this.data.data);
      this.procedureForm.patchValue(data);
    }
  }
  
}
