import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProcedureDialogComponent } from './create-procedure-dialog.component';

describe('CreateProcedureDialogComponent', () => {
  let component: CreateProcedureDialogComponent;
  let fixture: ComponentFixture<CreateProcedureDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProcedureDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProcedureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
