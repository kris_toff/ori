import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { Promotion } from "../promotion";
import { RemoteDialogData } from "../remote-dialog-data";

import _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'app-create-promotion-dialog',
  templateUrl: './create-promotion-dialog.component.html',
  styleUrls: ['./create-promotion-dialog.component.sass']
})
export class CreatePromotionDialogComponent implements OnInit {
  public promotionForm;
  
  public minDateTo: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: {data: RemoteDialogData, action: 'update' | 'create'}, 
  private dialogRef: MatDialogRef<CreatePromotionDialogComponent>) {
//    console.log('i',data);
  }

  ngOnInit() {
    this.promotionForm = new FormGroup({
      name: new FormControl(''),
      content: new FormControl(''),
      validity_date_from: new FormControl(''),
      validity_date_to: new FormControl('')
    });
    
    if (_.has(this.data, 'data')){
//    console.log(this.data);
      let data: Promotion = _.clone(this.data.data);
      this.transformDate(data);

      this.promotionForm.patchValue(data); 
    }
  }
  
  closeBox() {
//  console.log(this.data.content, this.promotionForm.value);
    let v = this.promotionForm.value;
    this.transformDate(v);
    
    let data = _.assign(this.data.data, v);
    this.dialogRef.close(data);
  }
  
  public changeDate(d) {
//    console.log('change', d);
    this.minDateTo = d.value;
  }
  
  private transformDate(data: Promotion): void {
    let handle = (v) => moment.isMoment(v) ? _.result(v, 'unix', null) : moment.unix(v);
    
    _.update(data, 'validity_date_from', handle);
    _.update(data, 'validity_date_to', handle);
  }
}