import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { BehaviorSubject, Observable } from "rxjs";
import { share, retry } from "rxjs/operators";

import { Procedure } from "./procedure";
import { Product } from "./product";

import _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url: string = '/api/products';
  private data$: BehaviorSubject<Product[]>;

  constructor(private http: HttpClient) {
    this.data$ = new BehaviorSubject([]);
  }
  
  getAll(): Observable<Product[]> {
    return this.data$.asObservable();
  }
  
  update(catalog: number, year: number, is_all: boolean = false): void {
    let params = _.zipObject(['catalog', 'year', 'is_all'], [catalog, year, _.toInteger(is_all)]);
    
    this.http
      .get('/api/products', {params: params})
      .pipe( retry(2) )
      .subscribe( (x: Product[]) => this.data$.next(x), () => this.data$.next([]));
  }
  
  load(data: any): Observable<Product[]> {
    let fdata: FormData = new FormData();
    _.each(data, (v, k) => {
      fdata.set(k, v);
    });
    
    let request = this.http.post<Product[]>(this.url + '/load', fdata);
    request = request.pipe( share() );
    
    request.subscribe();
    
    return request;
  }
}
